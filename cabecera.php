<?php   if (!isset($_SESSION)) { session_start(); } ?>
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <div class="container">
    <strong><a href="index.php" class="navbar-brand">INPRL</a></strong>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item" >
          <a href="inforiesgos.php" class="nav-link">Información sobre riesgos</a>
        </li>
        <li class="nav-item">
          <a href="nuevoparte.php" class="nav-link">Nuevo parte</a>
        </li>
        <li class="nav-item">
          <a href="modparte.php" class="nav-link">Modificar parte</a>
        </li>
        <li class="nav-item">
          <a href="consulta.php" class="nav-link">Consulta de partes</a>
        </li>
        <li class="nav-item">
          <?php if (isset($_SESSION["usuario"])) { ?>
            <span class="nav-link">Usuario <?php echo $_SESSION["usuario"]; ?> <a href="cerrar_sesion.php">Cerrar sesión</a> </span>
          <?php } else { ?>
            <a href="login.php" class="nav-link">Identifícate</a>
          <?php } ?>
        </li>
      </ul>
    </div>
  </div>
</nav>
