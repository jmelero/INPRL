<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>INPRL</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/inprl.css">
  </head>
  <body>
    <?php
    include("cabecera.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1 class="titulo1">Quienes somos</h1>
        </div>
        <div class="col-12">
          <p>
            El Instituto Nacional en Prevención de Riesgos Laborales es una instutición dedicada a dar soporte y en la plicación de la ley sobre la materia referente
             a los riesgos laborales.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <h1 class="titulo1">Contacto</h1>
        </div>
        <div class="col-12">
          <p>
             <strong>Correo:</strong>hola@inprl.com
          </p>
        </div>
        <div class="col-12">
          <p>
             <strong>Teléfono:</strong>960000000
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
              Nos encontramos en Av. de Catalunya S/N
        </div>
      </div>
      <div id="googleMap" style="width:100%;height:400px;"></div>
    </div>
    <script>
  function myMap() {
  var mapProp= {
      center:new google.maps.LatLng(39.478301,-0.354281),
      zoom:18,
  };
  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
  }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB591IgpdtTUHJkTGseYVL-YKjy9WAwV1Q&callback=myMap"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
