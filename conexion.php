<?php
/*
 * Copyright (C) Julián Melero Hidalgo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
require_once "credenciales.php";
// Creamos clase para la conexión
class conexion{
    public $mysql;
   function __construct(){
      // Creamos objeto mysqli
        $this->mysql = new mysqli(HOST,USER,PASS,DB);
        $this->mysql->query("SET NAMES 'utf8'");
        if (mysqli_connect_errno()) {
           die("No se ha podido conectar");
        }
    }
    //Función para ejecutar querys
    function query($query){
        if ( $res = mysqli_query($this->mysql,$query)){
            return $res;
        }
        else{
            return 1;
        }
    }

}

class trabajadores{
  function set_parte($dni,$fecha,$hora,$causa,$tipo,$partes,$gravedad,$baja,$comunidad,$edad,$sexo){
    //Comprobamos si los datos on correctos
    $error = 0;
    // Sacamos dia,mes y año de la fecha
    $valores_fecha = explode('/', $fecha);
    $anyo = $fecha[0].$fecha[1].$fecha[2].$fecha[3];
    $mes = $fecha[5].$fecha[6];
    $dia = $fecha[8].$fecha[9];

    if (empty($causa) || empty($tipo) || empty($partes) || checkdate($anyo,$mes,$dia) || !is_numeric($edad) ) {
      $error +=1;

    }
    if ($error>=1) {
      return 0;
    }
    $res = array();
    $conexion = new conexion();
    $sql="INSERT INTO parte (DNI,Fecha_accidente,Hora_accidente,Causa_accidente,Tipo_lesion,Partes_cuerpo_lesionado,Gravedad,Baja,comunidad,edad,sexo)
    values('$dni','$fecha','$hora','$causa','$tipo','$partes','$gravedad','$baja','$comunidad',$edad,'$sexo' )";
    $res[] = $conexion->query($sql);
    $res[] = mysqli_insert_id($conexion->mysql);
    return $res;
  }
  // Esta función devuelve el/los trabajadores solicitados
      function get_trabajadores(){
        // Creamos objeto de conexión para la clase trabajadores
        $conexion = new conexion();
        return $res = $conexion->query("SELECT * FROM trabajador");
      }
}


class partes{
  // Esta función devuelve el/los partes solicitados
    function get_partes($id_parte,$dni){
      $conexion = new conexion();
      return $res = $conexion->query("SELECT * FROM parte WHERE cod_parte=$id_parte and dni='$dni'");
    }
    function set_parte($fecha,$hora,$causa,$tipo,$partes,$gravedad,$baja,$cod_parte){
      //Comprobamos si los datos on correctos
      $error = 0;
      // Sacamos dia,mes y año de la fecha
      $valores_fecha = explode('/', $fecha);
      $anyo = $fecha[0].$fecha[1].$fecha[2].$fecha[3];
      $mes = $fecha[5].$fecha[6];
      $dia = $fecha[8].$fecha[9];

      if (empty($causa) || empty($tipo) || empty($partes) || checkdate($anyo,$mes,$dia) ) {
        $error +=1;

      }
      if ($error>=1) {
        return 0;
      }
      $conexion = new conexion();
      $sql="UPDATE parte SET Fecha_accidente='$fecha',Hora_accidente='$hora',Causa_accidente='$causa',Tipo_lesion='$tipo',
      Partes_cuerpo_lesionado = '$partes',Gravedad='$gravedad',Baja='$baja'
       WHERE cod_parte=$cod_parte";
      $res = $conexion->query($sql);
      return $res;
    }
    function del_parte($cod_parte){
      $conexion = new conexion();
      return $res = $conexion->query("DELETE FROM parte WHERE cod_parte=$cod_parte");
    }

    function consulta($dni,$fecha,$hora,$causa,$tipo,$partes,$gravedad,$baja,$comunidad,$edad,$sexo){
      $conexion = new conexion();
      $where = '';
      // Rellenamos el where
      if (!empty($fecha)) {
        $where.=" and p.Fecha_accidente='$fecha'";
      }
      if ($hora!=':00') {
        $where.=" and p.Hora_accidente='$hora'";
      }
      if (!empty($causa)) {
        $where.=" and p.Causa_accidente like '%$causa%'";
      }
      if (!empty($tipo)) {
        $where.=" and p.Tipo_lesion like '%$tipo%'";
      }
      if (!empty($partes)) {
        $where.=" and p.Partes_cuerpo_lesionado like '%$partes%'";
      }
      if (!empty($gravedad) and $gravedad!='-') {
        $where.=" and p.Gravedad ='$gravedad'";
      }
      if (!empty($baja) and $baja!='-') {
        $where.=" and p.Baja='$baja'";
      }
      if (!empty($edad)) {
        $where.=" and p.edad='$edad'";
      }
      if (!empty($comunidad) and $comunidad!='-' ) {
        $where.=" and p.comunidad='$comunidad'";
      }
      if (!empty($sexo) and $sexo!='-' ) {
        $where.=" and t.sexo='$sexo'";
      }
      $sql= "SELECT * FROM parte p
      INNER JOIN trabajador t ON p.DNI = t.DNI
      WHERE p.DNI='$dni' ";
      $sql.=$where." ORDER BY p.Fecha_accidente ASC";
      $res = $conexion->query($sql);      
      return $res;
    }

}

?>
