<?php
/*
 * Copyright (C) Julián Melero Hidalgo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

require_once "conexion.php";
$con = new conexion();
$partes = new partes();
$msg='';
if (isset($_POST['cod_parte_busqueda'])) {
  if (!isset($_SESSION)) {
    session_start();
  }
  $resultado = $partes->get_partes($_POST['cod_parte_busqueda'],$_SESSION["usuario"]);
  if ($resultado) {
    if (mysqli_num_rows($resultado)==0) {
      $msg =  "No existe el parte con el código ".$_POST['cod_parte_busqueda'] . " o bien no tiene acceso.";
    }
    while ($datos = mysqli_fetch_assoc($resultado)) {
      $codigo = $datos["cod_parte"];
      $dni = $datos["DNI"];
      $fecha = $datos["Fecha_accidente"];
      $hora= $datos["Hora_accidente"];
      $causa = $datos["Causa_accidente"];
      $tipo = $datos["Tipo_lesion"];
      $partes = $datos["Partes_cuerpo_lesionado"];
      $gravedad = $datos["Gravedad"];
      $baja = $datos["Baja"];
    }
  }
  else {
     $msg = "No se ha podido realizar la consulta";
  }
}

// Si se ha modificado
if (isset($_POST['modificar'])) {
  $accion = $partes->set_parte($_POST['Fecha_accidente'],$_POST['Hora_accidente'].":00",$_POST['Causa_accidente'],
  $_POST['Tipo_lesion'],$_POST['Partes_cuerpo_lesionado'],$_POST['Gravedad'],$_POST['Baja'],$_POST["cod_parte"]);
  $msg2 = "modificado";
}
// Si se ha modificado
if (isset($_POST['eliminar'])) {
  $accion = $partes->del_parte($_POST["cod_parte"]);
  $msg2 = "eliminado";
}
 ?>

 <!DOCTYPE html>
 <html lang="es">
   <head>
     <meta charset="utf-8">
     <title>INPRL</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="./css/inprl.css">
   </head>
   <body>
     <?php
     include("cabecera.php");
     if (!isset($_SESSION["usuario"])) {
       header("Location: login.php");
     }
     ?>
     <div class="container">
       <div class="row">
         <div class="col-12">
            <h1>Modificar Parte</h1>
         </div>
       </div>
       <div class="row">
         <div class="col">
           <?php
           if (isset($accion)) {
               if (!$accion) {
                 echo("<span class='badge badge-danger'>Error al ".$msg2." el parte. Contacte con su administrador.</span>");
               }
               else{
                 echo "<span class='badge badge-success'>Parte ".$msg2." con éxito.</span>";
               }
               unset($accion);
           }
           ?>
         </div>
       </div>
<div class="row">
  <div class="col-6 mt-3">
    <p class="info">Si desea modificar algún dato de un parte, por favor, introduzca el número de parte.</p>
  </div>
</div>
     <div class="row mt-3">

<div class="col-xs">
<!-- Formulario para la búsqueda -->
<form class="form" action="modparte.php" method="post">
  <div class="input-group ">
    <div class="input-group-prepend">
      <span class="input-group-text">Nº parte</span>
    </div>
    <input type="number" name="cod_parte_busqueda" class="form-control" required>
  </div>
  <br/>
  <div class="input-group ">
    <input type="submit" name="buscar" class="btn-info" value="Buscar">
  </div>
</form>

<?php

if (isset($dni)) {

?>

    <h2>Parte</h2>
     <form class="form" action="modparte.php" method="post">
       <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Código</span>
            </div>
             <input type="text" class="readonly" name="cod_parte" readonly value="<?php echo $codigo; ?>">
       </div>
    <div class="input-group">
         <div class="input-group-prepend">
           <span class="input-group-text">DNI</span>
         </div>
          <input type="text" name="dni" class="readonly" readonly value="<?php echo $dni; ?>">
    </div>
      <div class="input-group ">
        <div class="input-group-prepend">
          <span class="input-group-text">Fecha</span>
        </div>
        <input type="date" name="Fecha_accidente" class="form-control" value="<?php echo $fecha; ?>" required>
      </div>
      <div class="input-group ">
        <div class="input-group-prepend">
          <span class="input-group-text">Hora</span>
        </div>
       <input type="time" class="form-control" name="Hora_accidente" value="<?php echo $hora; ?>" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Causa Accidente</span>
       </div>
       <textarea name="Causa_accidente" class="form-control" rows="8" cols="80" required><?php echo $causa; ?></textarea>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Tipo lesión</span>
       </div>
       <input type="text" class="form-control" value="<?php echo $tipo; ?>" name="Tipo_lesion" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Partes del cuerpo lesionado</span>
       </div>
       <input type="text" name="Partes_cuerpo_lesionado" value="<?php echo $partes; ?>" class="form-control" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Gravedad</span>
       </div>
       <select class="form-control" name="Gravedad">
         <option value="Baja" <?php if ($gravedad=='Baja') {
           echo "selected";
         } ?>>Baja</option>
         <option value="Normal" <?php if ($gravedad=='Normal') {
           echo "selected";
         } ?> >Normal</option>
         <option value="Alta"  <?php if ($gravedad=='Alta') {
           echo "selected";
         } ?>>Alta</option>
       </select>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Baja</span>
       </div>
       <select class="form-control" name="Baja">
         <option value="Si" <?php if ($baja=='Si') {
           echo "selected";
         } ?>>Sí</option>
         <option value="No" <?php if ($baja=='No') {
           echo "selected";
         } ?>>No</option>
       </select>
     </div>
     <div class="form-group ">
       <input type="submit" name="modificar" class="btn-info" value="Modificar">
       <input type="submit" name="eliminar" class="btn-info" value="Eliminar">
     </div>

     </form>
     <?php
   }
   else {
        echo("<span class='badge badge-danger'>".$msg."</span>");
   }

     ?>
   </div>
</div>

</div>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   </body>
 </html>
