<?php
/*
 * Copyright (C) Julián Melero Hidalgo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/
require_once "conexion.php";
$con = new conexion();
$trab = new trabajadores();
if (isset($_POST['Causa_accidente'])) {

  $resultado = $trab->set_parte($_POST['dni'],$_POST['Fecha_accidente'],$_POST['Hora_accidente'].":00",$_POST['Causa_accidente'],
  $_POST['Tipo_lesion'],$_POST['Partes_cuerpo_lesionado'],$_POST['Gravedad'],$_POST['Baja'],$_POST['comunidad'],$_POST['edad'],$_POST['sexo']);


}
 ?>

 <!DOCTYPE html>
 <html lang="es">
   <head>
     <meta charset="utf-8">
     <title>INPRL</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="./css/inprl.css">
   </head>
   <body>
     <?php
     include("cabecera.php");
     if (!isset($_SESSION["usuario"])) {
       header("Location: login.php");
     }
     ?>
     <div class="container">
       <div class="row">
         <div class="col-12">
            <h1>Partes</h1>
         </div>
       </div>
       <div class="row">
         <div class="col">
           <?php
           if (isset($resultado)) {
               if (!$resultado[0]) {
                 echo("<span class='badge badge-danger'>Error al introducir el parte. Contacte con su administrador.</span>");
               }
               else{
                 echo "<span class='badge badge-success'>Parte introducido con éxito. El código del parte es el nº: ".$resultado[1].". Guarde este código para consultas futuras.</span>";
               }
               unset($resultado);
           }
           ?>
         </div>
       </div>
<div class="row">
  <div class="col-6 mt-3">
    <p class="info">Si desea enviar una incidencia para que sea solventada, por favor rellene el formulario con los datos que se detallan. En breve nos pondremos en contacto con usted.</p>
  </div>
</div>
     <div class="row mt-3">

<div class="col-xs">


     <form class="form" action="nuevoparte.php" method="post">

       <div class="input-group">

         <div class="input-group-prepend">
           <span class="input-group-text">DNI</span>
         </div>
          <select class="form-control" name="dni">
       <?php
       $res =  $trab->get_trabajadores();
       while ($datos = mysqli_fetch_assoc($res)) {
         echo "<option value='".$datos["DNI"]."'>".$datos["DNI"]."</option>";
       }
        ?>
      </select>
    </div>
      <div class="input-group ">
        <div class="input-group-prepend">
          <span class="input-group-text">Fecha</span>
        </div>
        <input type="date" name="Fecha_accidente" class="form-control" required>
      </div>
      <div class="input-group ">
        <div class="input-group-prepend">
          <span class="input-group-text">Hora</span>
        </div>
       <input type="time" class="form-control" name="Hora_accidente" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Causa Accidente</span>
       </div>
       <textarea name="Causa_accidente" class="form-control" rows="8" cols="80" required></textarea>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Tipo lesión</span>
       </div>
       <input type="text" class="form-control" name="Tipo_lesion" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Partes del cuerpo lesionado</span>
       </div>
       <input type="text" name="Partes_cuerpo_lesionado" class="form-control" required>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Gravedad</span>
       </div>
       <select class="form-control" name="Gravedad">
         <option value="Baja">Baja</option>
         <option value="Normal">Normal</option>
         <option value="Alta">Alta</option>
       </select>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Baja</span>
       </div>
       <select class="form-control" name="Baja">
         <option value="Si">Sí</option>
         <option value="No">No</option>
       </select>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Comunidad Autónoma</span>
       </div>
       <select name="comunidad" class="form-control">
        <option value="Andalucía">Andalucí­a</option>
        <option value="Aragón">Aragón</option>
        <option value="Principado de Asturias">Principado de Asturias</option>
        <option value="Islas Baleares">Islas Baleares</option>
        <option value="Paí­s Vasco">Paí­s Vasco</option>
        <option value="Canarias">Canarias</option>
        <option value="Cantabria">Cantabria</option>
        <option value="Castilla-La Mancha">Castilla-La Mancha</option>
        <option value="Castilla y León">Castilla y León</option>
        <option value="Catalunya">Cataluña</option>
        <option value="Extremadura">Extremadura</option>
        <option value="Galicia">Galicia</option>
        <option value="Comunidad de Madrid">Comunidad de Madrid</option>
        <option value="Región de Murcia">Región de Murcia</option>
        <option value="Comunidad Foral de Navarra">Comunidad Foral de Navarra</option>
        <option value="La Rioja">La Rioja</option>
        <option value="Comunidad Valenciana">Comunidad Valenciana</option>
        <option value="Ceuta">Ceuta</option>
        <option value="Melilla">Melilla</option>
      </select>
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Edad</span>
       </div>
       <input type="number" name="edad" class="form-control">
     </div>
     <div class="input-group ">
       <div class="input-group-prepend">
         <span class="input-group-text">Sexo</span>
       </div>
       <select class="form-control" name="sexo">
         <option value="hombre">Hombre</option>
         <option value="mujer">Mujer</option>
       </select>
     </div>

     <div class="form-group ">
       <input type="submit" name="eniar" class="btn-info" value="Enviar">
     </div>
     </form>
   </div>
</div>

</div>
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   </body>
 </html>
