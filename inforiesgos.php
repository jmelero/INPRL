<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>INPRL</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/inprl.css">
  </head>
  <body>
    <?php
    include("cabecera.php");
    ?>
  <div class="container indice">
    <h1>Índice</h1>
    <ul>
      <li>
        <a href="#normativa">Normativa básica</a>
      </li>
      <li>
        <a href="#condiciones">Condiciones de trabajo</a>
      </li>
      <li>
        <a href="#danyos">Daños derivados del trabajo</a>
      </li>
      <li>
        <a href="#riesgos">Riesgos</a>
      </li>
        <ul>
          <li>
            <a href="#">Peligro</a>
          </li>
          <li>
            <a href="#">Riesgo Laboral</a>
          </li>
          <li>
            <a href="#">Factores de riesgo</a>
          </li>
        </ul>
        <li>
          <a href="#tipologia">Tipología de peligros</a>
        </li>
        <li>
          <a href="#principios">Principios preventivos</a>
        </li>
        <li>
          <a href="#integracion">Integración de la prevención en la empresa</a>
        </li>
        <li>
          <a href="#riesgosdaw">Riesgos más importantes Desarrollador Web</a>
        </li>
        <li>
          <a href="#mesuras">Medidas preventivas</a>
        </li>

    </ul>
    <hr>
    <div class="row">
      <div class="col-md">
        <h2 id="normativa">Normativa básica</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md">
        <p>
          <div class="info">
            La LPRL quiere ser una ley marco, en el sentido de ser una ley que contiene todos los aspectos de la
            seguridad y salud laborales, aunque, como es lógico, de forma general.
          </div>
          <br>
          Esta Ley y sus normas de desarrollo son de aplicación tanto en el ámbito de las relaciones laborales reguladas en el texto refundido de la Ley del Estatuto de los Trabajadores, como en el de las relaciones de carácter administrativo o estatutario
del personal civil al servicio de las administraciones  públicas,  con  las  peculiaridades  que, en este caso, se consideran en
esta Ley o en sus normas de desarrollo.
<br>
Todo esto sin perjuicio para el cumplimiento de las obligaciones específicas que se establecen para fabricantes, importadores y suministradores, y de los
derechos y las obligaciones específicas que pueden derivarse para los trabajadores autónomos. También son aplicables a las sociedades cooperativas,
constituidas de acuerdo con la legislación que les sea de aplicación, en las que haya socios cuya actividad consista en la prestación de su trabajo personal, con las particularidades derivadas
de su normativa específica.
<br>
Cuando se hace referencia en esta Ley a los trabajadores y empresarios, setiene que entender que también están comprendidos:
<ol>
  <li>
El personal civil con
una relación de carácter administrativo o estatuario
y la administración pública a la que pr
esta servicios, en los términos expre
sados en la Disposición adic
ional tercera de esta Ley.
</li>
<li>
Los socios de las cooperativas y la
s sociedades cooperativas a las que
prestan sus servicios.
Esta
Ley no es de aplicación
en aquellas actividade
s cuyas particularida
des lo impiden en el
 ámbito de las func
iones públicas de:
<ol>
<li>
Policía, seguridad y resguardo aduanero
</li>
<li>
Servicios operativos de protección civil y peritaje forense en los casos
de riesgo grave, catást
rofe y calamidad pública.
</li>
<li>
En la relación laboral de carácter especial al servicio del hogar familiar.
</li>
<li>
En los centros y establecimientos mili
tares es de aplicación lo que dispone esta Ley, con las particularidades
 prev
istas en su normativa específica.
</li>

En los estable
cimientos penitencia
rios, se deben adaptar a esta Ley
aquellas actividades cuyas características justifiquen una regulación especial.
</ol>
</li>
</ol>
Pero, aunque se trata de una ley que
busca la prevención antes que nada,
su articulado no puede descansar excl
usivamente en la ordenación de las
obligaciones y responsabilidades de lo
s actores directamen
te relacionados
con el hecho laboral.
<div class="info">
  El propósito de fomentar una auténtica cultura preventiva, mediante
la promoción de la mejora de la educación en la me
ncionada materia en todos los ámbitos educativos, invo
lucra a la sociedad en su conjunto y constituye uno de los objetos básicos y de efectos, tal vez, más
trascendentales para el futuro de
los objetivos perseguidos por la Ley.
</div>

Para conseguir esta cultura preventiva, las Administraciones públicas tienen que promover la mejora de la educación en materia preventiva en los
diferentes niveles de enseñanza; por consiguiente, en el ámbito de la Administración General del Estado, conforme al artículo 5.2, se deberá establecer  una  colaboración  permanente  entre  el  Ministerio  de  Trabajo  y
Asuntos Sociales y los ministerios que correspondan, en particular los de Educación y Ciencia y de Sanidad y Consumo.
        <br>
          Puede encontrar información detallada sobre la ley en el <a href="https://www.boe.es/diario_boe/txt.php?id=BOE-A-1995-24292" target="_blank">BOE</a>.
        </p>
      </div>
    </div>

    <div class="row">
    <div class="col-md">
      <h2 id="condiciones">Condiciones de trabajo</h2>
    </div>
    </div>
    <div class="row">
    <div class="col-md">
    <p>
    Cuando consideramos a la persona en su medio o actividad laboral, la situamos en unas condiciones de trabajo, es decir, rodeado de una serie
    de elementos y circunstancias propias de esa condición. Estas condiciones de trabajo suponen, con frecuencia, riesgos potenciales,
    de ahí la necesidad de conocerlas para así determinar mejor los riesgos a que está expuesta la persona en su actividad diaria.
    Estas condiciones de trabajo se clasifican en:
    <ul>
    <li><strong>Mecánicas:</strong> aquí hemos de ver todos los elementos, materiales, uten
    silios y equipos del trabajo: el local y su estructura, las instalaciones, las
    máquinas, sus movimientos, las energías como la eléctrica, la dinámi
    ca o la térmica; la inercia, las cargas, presiones, tensiones y pesos; las
    herramientas, los materiales que se trabajan, el mobiliario, ropa, etc.
    </li>
    <li>
    <strong>Físicas:</strong> el espacio del lugar de trabajo, los accesos y tránsitos, la luz,
    el ruido, la temperatura, la humedad, la altitud, la presión, las vibra
    ciones, las radiaciones.
     Obligaciones y responsabilidades en materia de prevención de riesgos...
    19
    </li>
    <li>
    <strong>Químicas:</strong> debemos tener en cuenta aquí la presencia de cualquier
    sustancia, ya sea en estado sólido, líquido o gaseoso: el aire y su con
    tenido de oxígeno o cualquier otra materia, sea o no contaminante,
    las partículas suspendidas como polvos, fibras o nieblas, los produc
    tos que se manejan o se producen en el proceso de trabajo: pinturas,
    disolventes, medicamentos, plaguicidas agrícolas, tintes, aceites, etc.
    que se generan como consecuencia de humos, vapores, residuos tó
    xicos...
    Hemos de considerar estas sustancias químicas bajo dos aspectos: pri
    mero, porque pueden causar incendios o explosiones por sus carac
    terísticas de volatilidad, inflamabilidad y reacciones potencialmente
    peligrosas. Y segundo, porque pueden provocar enfermedades en las
    personas expuestas.
  </li>
  <li>
    <strong>Biológicas:</strong> cualquier agente vivo que pueda afectar a la persona en su
    trabajo de forma directa o indirecta. La presencia de hongos, bacterias
    o virus pueden causar enfermedades psíquicas.
  </li>
  <li>
    <strong>Psicológicas:</strong> en el trabajo hay condiciones que pueden influir sobre la
    mente de forma directa o indirecta. Unas veces, porque hay personas
    que pueden verse deprimidas por un trabajo monótono, rutinario o
    desmotivadas y decepcionadas por depender su intervención personal
    de las máquinas o la electrónica. En otros, se provoca fatiga mental o
    estrés por exceso de actividad, responsabilidad, atención constante,
    prisas, etc.
  </li>
  <li>
    <strong>Sociales:</strong> se consideran aquí los aspectos humanos dentro de la em
    presa, por su dedicación, organización, trato con sus empleados, co
    municaciones y relaciones entre las personas y entre los diferentes ni
    veles, secciones e individuos, los estilos de mando, la participación en
    los objetivos, la integración en la empresa y equipos de trabajo, las
    remuneraciones, el reconocimiento de los logros y avances, la seguri
    dad en el empleo.
  </li>
  </ul>
    <span> Sacado de los recursos de Seminario Prevención</span>
    </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md">
      <h2 id="danyos">Daños</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md">
    <p>
    Es importante saber que, ante cualquier peligro para la seguridad y/o la
    salud, si se quiere ser eficaz, lo primero que hay que intentar es evitar los
    riesgos y evaluar los que no se puedan evitar. Los riesgos evaluados se
    deben combatir en su origen y, así, ir aplicando los principios generales
    de la acción preventiva indicados en el artículo 15 de la Ley de Prevención
    de Riesgos Laborales.
    <h4>Accidente de trabajo</h4>
    Se entiende por accidente de trabajo toda lesión corporal que el tra
    bajador sufra con ocasión o a consecuencia del trabajo que ejecuta
    por cuenta ajena.
  </p>
</div>
</div>
  <div class="row">
    <div class="col-md">
      <p>
      Esta definición legal se refiere tanto a las lesiones que se producen en el
  centro de trabajo como a las producidas en el trayecto habitual entre éste y
  el domicilio del trabajador. Estos últimos son los considerados accidentes
  initinere o en desplazamiento (cuando vamos de nuestro domicilio al trabajo
  y viceversa).
  En los accidentes de trabajo podemos tener en cuenta, fundamentalmente,
  varios factores para su consideración:
  <ul>
  <li>La probabilidad, que se analizará teniendo en cuenta la doble vertiente
  de: probabilidad de que ocurra un accidente, probabilidad de que se
  produzcan lesiones.
</li>
<li>
  La gravedad o severidad de los daños, que atenderá a la calificación
  de lesiones, para lo que se tendrán en cuenta las consecuencias que se
  prevea pueda provocar, o el número de trabajadores a los que puede
  afectar.
</li>
<li>
  La frecuencia con la que se repita el riesgo o el tiempo de exposición
  a que el trabajador o trabajadores estén expuestos.
</li>
</ul>
La seguridad en el trabajo constituye una técnica preventiva que actúa
sobre el entorno físico que rodea al trabajador para eliminar o proteger
aquellas situaciones peligrosas e inseguras, con el fin de reducir el riesgo
de accidente de trabajo y controlar sus posibles consecuencias.
</p>
  </div>
  </div>
  <div class="row">
    <div class="col-md">
    <p>
    <h4>Enfermedad Laboral</h4>
    Art. 84: la Ley General de la Seguridad Social la define como toda en
fermedad contraída a consecuencia del trabajo ejecutado por cuen
ta ajena, en las actividades que se especifiquen en el cuadro del Real
decreto 1299/2006 y las que se aprueben por las disposiciones de
aplicación y desarrollo de esta ley, que estén provocadas por la ac
ción de los elementos o sustancias que en dicho cuadro se indiquen
para toda enfermedad profesional.
  </p>
</div>
</div>
  <div class="row">
    <div class="col-md">
      <h2 id="riesgos">Riesgos</h2>
      <p>
        <ul>
          <li>
        <strong>Concepto de peligro:</strong> fuente o situación con capacidad de daño
en términos de lesiones, daños a la propiedad, daños al medio
ambiente o una combinación de ellos.
        </li>
        <li>
<strong>Concepto de riesgo laboral:</strong> posibilidad de que un trabajador
sufra un determinado daño derivado del trabajo.
Daños derivados del trabajo: enfermedades, patologías o lesio
nes sufridas con motivo u ocasión de un trabajo.
</li>
<li>
  Un <strong>factor de riesgo laboral</strong> es el elemento o conjunto de elementos que, estando presentes en las condiciones de trabajo pueden desencadenar una disminución en la salud del trabajador, pudiendo causar un daño en el ámbito laboral.
</li>
</ul>
      </p>
    </div>
  </div>

<div class="row">
  <div class="col-md">
    <h2 id="tipologia">Tipología de riesgos</h2>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <p>
      <ul>
    <li>Riesgos Físicos:

    Definimos ruido a una sensación auditiva generalmente desagradable. Cuando escuchamos un ruido primero lo apreciamos por el oído externo, después, la onda es recibida por el oído medio que es donde esta el tímpano. Posteriormente la señal pasa por una cadena de huesecillos y la recibe el cerebro mediante unas células capilares.Cuando el ruido es muy fuerte se activan las células capilares y hay riesgo de perder la capacidad auditiva. Debemos protegernos con todas las medidas de seguridad posibles.
    Cuando el ruido es muy fuerte se activan las células capilares y hay riesgo de perder la capacidad auditiva. Debemos protegernos con todas las medidas de seguridad posibles.
    Las vibraciones por todo tipo de maquinaria pueden afectar a la columna vertebral, dolores abdominales y digestivos, dolores de cabeza…
    El deslumbramiento, las sombras, la fatiga y el reflejo son factores producido por la iluminación. Estos elementos pueden producir un accidente por eso hay que vigilar con el tipo de lámparas y respetar los niveles adecuados de luz.
    La temperatura y la humedad en el ambiente si son excesivamente altas o bajas pude producir efectos adversos en las personas. Los valores ideales en el trabajo son 21ºC y 50% de humedad.
    Las radiaciones ionizantes son ondas electromagnéticas que alteran al estado físico sin percibirse en el ambiente. Los efectos son graves a la larga, por eso hay que limitar las ondas y tener un control médico.
</li>
<li>
Riesgos Químicos:

    Son producidos por procesos químicos y por el medio ambiente. Las enfermedades como las alergias, la asfixia o algún virus  son producidas por la inhalación, absorción, o ingestión. Debemos protegernos con mascarillas, guantes y delimitar el área de trabajo.
</li>
<li>
Riesgos Biológicos:

    Las enfermedades producidas por los virus, bacterias, hongos, parásitos son debidas al contacto de todo tipo de ser vivo o vegetal. Para evitarlas se recomienda tener un control de las vacunas y sobretodo protegerse con el equipo adecuado.
</li>
<li>
Riesgos Ergonómicos:

    La ergonomía es la ciencia que busca adaptarse de manera integral en el lugar de trabajo y al hombre. Los principales factores de riesgo ergonómicos son: las posturas inadecuadas, el levantamiento de peso, movimiento repetitivo. Puede causar daños físicos y molestos.
    Este tipo de riesgo ofrece cifras relativamente altas ocupando el 60% de las enfermedades en puestos de trabajos y el 25% se deben a la manipulación de descargas. Cuando levantamos peso la espalda tiene que estar completamente recta y las rodillas flexionadas. Si son trabajos físicos, antes de empezar debemos estirar los músculos y las articulaciones para evitar futuras lesiones. Hay que utilizar métodos seguros en todo momento.
</li>
<li>
Riesgos Psicosociales:

    Algunos de estos riesgos nos a afectan a todos nosotros en algún momento de nuestra vida laboral. Algunos de los más comunes son: estrés, fatiga, monotonía, fatiga laboral… Para prevenirlas es recomendable respetar los horarios laborales sin excederse en las horas.
    Debemos tener como mínimo un descanso de 15 minutos  a partir de las 6 horas. La estabilidad y un buen ambiente nos ayudaran a disminuir estos riesgos.
</li>
<li>
Riesgos Mecánicos:

    Este tipo de riesgos se ven reflejados a trabajos en altura, superficies inseguras, un mal uso de las herramientas, equipos defectuosos. Debemos asegurarnos siempre de revisar la maquinaria en la que trabajamos para evitar posibles incidentes.
</li>
<li>
Riesgos Ambientales:

    Estos factores son los únicos que no podemos controlar. Se manifiestan en la naturaleza la lluvia, la tempestad, las inundaciones… Debemos ser previsibles y prudentes.
  </li>
</ul>
    </p>
  </div>

</div>

<div class="row">
  <div class="col-md">
    <h2 id="principios">Principios preventivos</h2>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <p>
      La Ley 31/1995 de Prevención de Riesgos Laborales determina el marco
por el que han de regirse las empresas en todo lo relacionado con la pre-
vención de riesgos, teniendo, entre otros objetivos, el de promover la se-
guridad y la salud de los trabajadores mediante la aplicación de medidas
y el desarrollo de las actividades necesarias para tal fin.
<br>
La ley define los principios generales de la acción preventiva (art. 15) que
deberá aplicar el empresario y que son:
    </p>

    <div class="info">
    <ul>
  <li>  Evitar los riesgos.</li>
<li>Evaluar los riesgos que no se puedan evitar.</li>
<li>  Combatir los riesgos en su origen.</li>
<li>  Adaptar el trabajo a la persona.</li>
<li>Planificar la prevención.</li>
<li>  Adoptar medidas que antepongan la protección colectiva frente
a la personal.</li>
<li>  Dar las debidas instrucciones a los trabajadores.</li>
<li>Permitir la participación de los trabajadores.</li>
<li>Informar y consultar a los trabajadores.</li>
<li>Dar formación teórica y práctica suficiente y adecuada en materia preventiva.</li>
</ul>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md">
    <h2 id="integracion">Integración de la prevención en la empresa</h2>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <ol>
      <li>Evitar los riesgos:
    Este primer principio supone eliminar, cuando sea posible y no suponga elimi
nar la actividad productiva, los elementos que son generadores de los riesgos.
Por ejemplo, si en un proceso productivo se producen accidentes y enferme
dades profesionales debidos a la necesidad de manipular manualmente cargas
pesadas, mecanizando totalmente dicho proceso evitaremos la exposición de
los trabajadores y por tanto ese riesgo en concreto.
</li>
<li>
 Evaluar los riesgos que no se puedan evitar:
La evaluación de riesgos permite determinar la magnitud de los riesgos labo
rales que no hemos podido evitar. Igualmente, nos sirve para determinar si es
necesario aplicar medidas preventivas y con qué prioridad.
Un ejemplo es la evaluación de riesgos por exposición a niveles peligrosos
de ruido; en función del resultado de las mediciones técnicas de ruido, sabre
mos si no es necesario tomar medidas preventivas o si deberemos actuar al
respecto: elaborar un plan de reducción de ruido, aislar o alejar las fuentes
de ruido, apantallar las zonas afectadas, decidir qué equipos de protección
auditiva proveer a los trabajadores, los reconocimientos médicos a realizar, la
periodicidad de revisión de las mediciones y los controles a efectuar, etc.
</li>
<li>
 Combatir los riesgos en su origen:
Se debe buscar la causa de la existencia del peligro que determina el riesgo
laboral y eliminarla o controlarla.
Continuando con el ejemplo anterior, en aplicación de este principio, debe
mos estudiar de dónde procede el ruido y por tanto cuál es su origen, de
modo que las medidas preventivas y de control se tomen ahí para eliminar,
si es posible, o controlar de no serlo, el problema en la fuente.
</li>
<li>
 Adaptar el trabajo a la persona:
Es necesario adaptar el trabajo, las condiciones de trabajo, a las características
personales de los trabajadores, en particular en lo que respecta a la definición
de los puestos de trabajo, así como a la elección de los equipos y los méto-
dos de trabajo y de producción. Todas las condiciones de trabajo deben es-
tablecerse considerando las características individuales de los trabajadores de
modo que se adapten en la mayor medida posible y de la forma más segura
y saludable a éstos.
El hecho de proporcionar equipos de trabajo (máquinas, herramientas, vehí-
culos, etc.) y equipos de protección (calzado de protección, guantes homo-
logados, pantallas faciales, etc.) ajustados a las características físico-psíquicas
de los trabajadores es un ejemplo claro de la aplicación de este principio.
</li>
<li>
 Planificar la prevención:
Las actuaciones preventivas tienen que seguir un orden lógico, que nos pro
porcionará la evaluación de riesgos laborales: empezaremos a realizar las ac
ciones que reduzcan los riesgos calificados como más graves detectados en
la evaluación.
Un ejemplo claro es el Plan de prevención que establece como obligatorio la
Ley 54/2003 de Reforma del marco normativo de la prevención.
</li>
<li>
 Adoptar medidas que antepongan la protección colectiva frente a la per
sonal:
Este principio determina que la protección individual sólo se usará cuando
el riesgo no se haya podido reducir suficientemente mediante medidas de
protección colectiva.
Por ejemplo, en caso de exposición a productos químicos peligrosos, siempre
se antepondrán los medios de protección colectiva, como extracciones gene
rales de aire, previamente al uso, en exclusiva, de mascarillas o equipos de
protección respiratoria individuales.
</li>
<li>
 Dar las debidas instrucciones a los trabajadores:
La instrucción de los trabajadores es un aspecto clave en la prevención de
riesgos laborales.
El principio se cumple, por ejemplo, cuando se incorpora un nuevo trabajador
a una empresa o cuando cambia de puesto de trabajo, se le dan nuevas tareas
o equipos de trabajo; en ese caso hay que darles las instrucciones preventivas,
necesarias para que conozca los riesgos laborales de su puesto de trabajo y las
medidas preventivas a adoptar, de modo que pueda desempeñar su trabajo
de forma segura y saludable.
</li>
<li>
 Permitir la participación de los trabajadores:
La participación es un factor fundamental para lograr la integración de la pre
vención de riesgos laborales en las empresas. Mediante la aplicación de este
principio, se consigue la implicación de todos los trabajadores en el cumpli
miento de los objetivos de la Ley de Prevención.
Una situación clara de esta posibilidad de participación es la de la potestad de
designar en la empresa a representantes de los trabajadores, específicos para
cuestiones preventivas cuando se nombra a los delegados de prevención que
se integrarán en el comité de seguridad y salud de la empresa.
</li>
<li>
 Informar y consultar a los trabajadores:
La información y consulta a los trabajadores, en materia de prevención de
riesgos laborales, es esencial para evitar los accidentes y las enfermedades
profesionales.
Como ya hemos comentado, en el caso del principio de instruir debidamente
a los trabajadores.
Es de sentido común la consulta a los trabajadores, personalmente o por me
dio de sus representantes en cuestiones de prevención de riesgos laborales
(delegados de prevención o miembros de comité de seguridad y salud) sobre
todas aquellas cuestiones que sean relevantes para la adecuada gestión de
los riesgos.
A la hora de determinar qué medidas preventivas aplicar para reducir un ries
go, la información que al respecto pueda proporcionar el trabajador es espe
cialmente valiosa para que los responsables de prevención de una empresa
adopten la solución técnico-preventiva más eficiente (decidir, por ejemplo,
qué tipo de equipo de protección individual utilizar frente a un riesgo con
creto: cascos o tapones en el supuesto de ruido).
</li>
<li>
 Dar formación teórica y práctica suficiente y adecuada en materia pre
ventiva:
La formación, junto con la información y la consulta a los trabajadores en
prevención de riesgos Laborales, resulta básica para lograr la eliminación de
accidentes y enfermedades en el trabajo.
Está formación siempre tendrá un elemento teórico, pero deberá ir acompa
ñada, cuando proceda, de instrucciones prácticas (por ejemplo, al formar a
los trabajadores en la extinción de incendios, deberemos realizar prácticas in
situ de uso de extintores).
</li>
</ol>
  </div>
</div>

<div class="row">
  <div class="col-md">
    <h2 id="riesgosdaw">Riesgos más importantes vinculados a un sitio de trabajo de Desarrollador de Páginas Web</h2>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <ol>
    <li>CAÍDAS AL MISMO NIVEL</li>
    <li>CAÍDAS A DISTINTO NIVEL</li>
    <li>GOLPES CONTRA OBJETOS</li>
    <li>TRASTORNOS MUSCULOESQUELÉTICOS</li>
    <li>MANEJO MANUAL DE CARGAS</li>
    <li>APLASTAMIENTO POR VUELCO DE MATERIAL DE OFICINA</li>
    <li>GOLPES Y/O CORTES CON HERRAMIENTAS</li>
    <li>CONTACTO ELÉCTRICO</li>
    <li>INCENDIOS</li>
    <li>FATIGA VISUAL</li>
    <li>CONFORT ACÚSTICO</li>
    <li>CONFORT TÉRMICO</li>
    <li>FACTORES PSICOSOCIALES</li>
</ol>
  </div>
</div>
<div class="row">
  <div class="col-md">
    <h2 id="mesuras">Medidas preventivas más relevantes aplicables a un entorno laboral de perfil profesional DAW </h2>
  </div>
</div>

<div class="row">
  <div class="col-md">
    <ol>
  <li>CAÍDAS AL MISMO NIVEL
    <br>
    <ul>
Las medidas preventivas que adoptaremos serán:
<li>Los suelos de los locales de trabajo deben ser fijos,
estables y no resbaladizos, sin irregularidades ni
pendientes peligrosas.</li>
<li>Se debe canalizar todo el cableado de ordenado
res y demás instalaciones.</li>
<li>Mantener las zonas de paso despejadas.</li>
<li>Concienciar a los trabajadores del mantenimiento
del orden y la limpieza de sus puestos de trabajo.</li>
<li>Utilizar calzado con suelas antideslizantes (con al
gún tipo de dibujo, no lisos).</li>
<li>Marcar y señalizar los obstáculos que no puedan
ser eliminado.</li>
</ul>
</li>
<li>
CAÍDAS A DISTINTO NIVEL
<br>
Las medidas preventivas que adoptaremos serán:
<ul>
<li>Las escaleras de mano deben subirse con precaución, siempre de frente a ellas, tanto al subir como
al bajar agarrándose con las dos manos.</li>
<li>La escalera debe estar abierta al máximo según el elemento limitador de abertura (cuerda, cadena,
etc), o se apoyará sobre la pared intentando que
el ángulo de inclinación esté comprendido entre
70 y 75°.</li>
<li>Facilitar el acceso a zonas de almacenamiento elevadas mediante escaleras fijas o móviles perfectamente aseguradas.</li>
</ul>
</li>
<li>
GOLPES CONTRA OBJETOS
<br>
Las medidas preventivas que adoptaremos serán:
<ul>
<li>Mantener despejados de objetos los pasillos y las
zonas de paso.</li>
<li>Concienciar a los trabajadores del mantenimiento
del orden y la limpieza de sus puestos de trabajo.</li>
<li>Comprar equipos de trabajo seguros, que tengan
el marcado CE.</li>
</ul>
</li>
<li>
TRASTORNOS MUSCULOESQUELÉTICOS
<br>
<ul>
  <li>
  Las mesas de trabajo debe ser lo suficientemente
amplia y espaciosa para que en ella se puedan de
positar cómodamente todos los utensilios necesarios para el dasarrollo de la tarea. Las dimensiones
aproximadas deben ser: 160 cm de ancho por 80
cm de alto y 100 cm de profundidad.
</li>
<li>El espacio libre para las extremidades debe ser de
60 cm.</li>
<li>La silla debe ser ajustable en altura del asiento e
inclinación y altura del espaldo. Debe ser giratoria
con 5 puntos de apoyo y ruedas que permitan su
desplazamiento  fácilmente.  Se  recomienda  que
tenga apoyabrazos.</li>
<li>El reposapiés debe ser móvil para permitir al usuario colocarlo según sus necesidades de cada momento. Su utilización se recomienda, únicamente,
cuando al ajustar la silla a la altura de la mesa de
trabajo, las piernas de la persona puedan quedar
colgando.</li>
<li>La inclinación debe ser ajustable entre 0 y 15° y
debe poderse ajustar en altura entre o y 25 cm. Su
tamaño debe permitir apoyar los pies con holgura
(45x35 cm).</li>
<li>Como regla general la pantalla debe estar a unos
40 cm y la parte superior de la pantalla al altura de
los ojos.</li>
<li>El teclado será de color claro y mate, con los caracteres en negro bien diferenciados. Expandido y
con reposamuñecas.</li>
<li>El puntero debe tener forma adaptable a la palma
de la mano. Se debe trabajar con la mano apoyada
sobre el rátón, la muñeca recta y el codo apoyado
sobre la mesa formando un ángulo de 90°. Los botones deben ser sensibles y de tacto agradable.</li>
<li>El portadocumentos se debe utilizar en puestos de
trabajo donde la tarea principal consista en pasar
datos con documentos de los que se tengan que
extraer muchos datos. Debe ser giratorio, móvil e
inclinable.</li>
<li>Realizar ejercicio físico aprovechando las pausas
de la jornada.</li>
<li>Establecer pausas planificadas, cuya duración dependerá de las exigencias concretas de cada tarea.</li>
</ul>
</li>
<li>
  APLASTAMIENTO POR VUELCO DE MATERIAL DE
  OFICINA
<br>
<ul>
  <li>Se anclarán a la pared archivadores y estanterías.</li>
  <li>Los  archivadores  deberán  llevar  un  sistema  que
  trabe los cajones de tal manera, que sólo sea posible tener uno abierto. </li>

</ul>
<li>
GOLPES Y/O CORTES CON HERRAMIENTAS
<br>
<ul>
  <li>Comprar  máquinas  y  herramientas  seguras,  que
tengan el marcado CE.</li>
<li>Cumplir las normas de seguridad indicadas por el
fabricante.</li>
<li>Guardar las herramientas cortantes en fundas y/o
soportes adecuados.</li>
<li>En general, se utilizarán las herramientas de acuerdo con su función, y de una manera prudente.</li>
<li>Las herramientas se mantendrán en buen estado.</li>
<li>Utilizar equipos de protección individual con marcado CE.</li>
</ul>
</li>
<li>
CONTACTO ELÉCTRICO
<br>
<ul>
<li>Como norma general la instalación deberá ejecutarse de acuerdo a la reglamentación vigente.</li>
<li>La  instalación  eléctrica  dispondrá  de  protección
magnetotérmica, diferencial y toma de tierra.</li>
<li>En caso de avería, comunicar los daños y la reparación será efectuada por personal especializado.</li>
<li>Las herramientas eléctricas utilizadas deberán llevar el marcado CE.</li>
<li>Evitar sobrecargar los enchufes con ladrones.</li>
<li>Utilizar para los elementos portátiles tensiones de
seguridad.</li>
<li>Realizar un mantenimiento periódico de las instalaciones por instalador autorizado.</li>
<li>No utilizar aparatos eléctricos con las manos húmedas.</li>
<li>Separar el cableado de las fuentes de calor.</li>
<li>No realizar operaciones de mantenimiento de los</li>
<li>equipos electrónicos sin desconectarlos de la red.</li>
<li>Las tapas de los cuadros eléctricos deben permanecer cerradas y señalizado el peligro eléctrico.</li>
</ul>
</li>
<li>
INCENDIOS
<br>
<ul>
  Las medidas preventivas que adoptaremos serán:
<li>Existencia de extintores de incendios adecuados a
la clase de fuego.</li>
<li>Mantenimiento periódico de extintores y demás
equipos contra incendios.</li>
<li>Revisar y mantener las instalaciones eléctricas aisladas y protegidas.</li>
<li>Instalar sistemas de detección y alarma.</li>
<li>Señalizar las zonas de riesgo de incendio.</li>
<li>Señalizar y dejar libres las salidas de emergencia.</li>
<li>Realizar planes de emergencia e implantarlos.</li>
</ul>
</li>
<li>
FATIGA VISUAL
<ul>
  Las medidas preventivas que adoptaremos serán:
<li>Se graduará el brillo y el contraste mediante los
mandos de la pantalla.</li>
<li>Se colocará la pantalla de tal manera que están
situadas paralelamente a ella las fuentes de iluminación.</li>
</ul>
</li>

<li>
CONFORT ACÚSTICO
<br>
<ul>
  Las medidas preventivas que adoptaremos serán:
<li>Se regularán los timbres de los teléfonos.</li>
<li>El ruido ambiental no debe superar los 50 db (A).</li>
</ul>
</li>

<li>
CONFORT TÉRMICO
<br>
<ul>
  Las medidas preventivas que adoptaremos serán:
<li>La temperatura de los locales se mantendrá entre
17 y 27 °C, y la humedad relativa entre el 30 y el
70%.</li>
<li>Se instalarán si es necesario sistemas de aire acondicionado que mantengan la temperatura de los
locales entre estos valores.</li>
</ul>
</li>
<li>
FACTORES PSICOSOCIALES
<br>
<ul>
  Las medidas preventivas que adoptaremos serán:
<li>Se intentará que el trabajador tenga la máxima información sobre la totalidad del proceso en el que
está trabajando.</li>
<li>Distribuir claramente las tareas y competencias.</li>
<li>Planificar los diferentes trabajos de la jornada, teniendo en cuenta una parte para imprevistos.</li>
<li>Realizar pausas o alternancia de tareas para evitar
la monotonía del trabajo.</li>
</ul>
</li>


</ol>


  </div>
</div>

    </div>
  </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
