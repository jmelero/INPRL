<?php
/*
 * Copyright (C) Julián Melero Hidalgo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>INPRL- Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/inprl.css">
  </head>
  <body>
    <?php
    include("cabecera.php");
    require_once "autenticacion.php";

    if ($_POST) {
      $auth = new autenticacion();
      $respuesta = $auth->exist_user($_POST["dni"],$_POST["pwd"]);
      if ($respuesta==1) {
          header("Location: index.php");
      }
    }
    ?>
    <div class="container">
      <div class="row">
        <div class="col-12">
           <h1>Autenticación</h1>
        </div>
      </div>
      <div class="col-xs">
      <!-- Formulario para la búsqueda -->
      <form class="form" action="login.php" method="post">
        <div class="input-group ">
          <div class="input-group-prepend">
            <span class="input-group-text">DNI</span>
          </div>
          <input type="text" name="dni" maxlength='12' class="form-control" placeholder="Ej: 12345678A" required>
        </div>
        <div class="input-group ">
          <div class="input-group-prepend">
            <span class="input-group-text">Contraseña</span>
          </div>
          <input type="password" name="pwd" class="form-control"  required>
        </div>
        <br/>
        <div class="input-group ">
          <input type="submit" name="login" class="btn-info" value="Entrar">
        </div>
      </form>
    </div>
    </div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
